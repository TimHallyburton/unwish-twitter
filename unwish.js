const wishlist_match = /^amazon(.+)\/wishlist*/;

const domObserver = new MutationObserver(function (mutations) {
  mutations.forEach(function (mutation) {
    if (mutation.type === "childList") {
      for (let span of document.getElementsByTagName("span")) {
        if (span.innerText.match(wishlist_match)) {
          span.remove();
        }
      }
    }
  });
});

const observerConfig = {
  attributes: true,
  childList: true,
  characterData: true,
  subtree: true,
};

domObserver.observe(document.body, observerConfig);
