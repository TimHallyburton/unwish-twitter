# Unwish Twitter

A simple browser add-on to remove Amazon wishlists from your Twitter experience.

## Installation

The preferred way of installation is via your browsers extension marketplace (coming soon, this process takes some time).
First of all you need to download the latest version from [here](https://codeberg.org/TimHallyburton/unwish-twitter/releases/tag/1.0.0-pre).

### Firefox
 * Open a new browser window
 * Navigate to `about:addons`
 * From the left hand side menu choose "Extensions"
 * Click on the cogwheel symbol -> "Install Add On from file..."
 * Select the downloaded zip file


### Chrome
 * From the settings menu choose "Settings"
 * Select "Extensions" from the left hand side menu
 * On the top right corner choose "Activate Developer Mode"
 * Click "Load unpacked"
 * Select the downloaded zip file

 ## Building

 Should you decide to build this extension yourself follow these steps
 * Clone this repository
 * `npm run build`
 
 You should fine a zip file in the `/dist` folder containing the latest version. Now follow the installation process.

## Feedback and Contributions

Any feedback and contributions are warmly welcome.
It would be much appreciated if someone could check this addon for Windows and MacOS + Safari browser.

## License
"Unwish Twitter" is licensed under the MIT license. A copy is provided within the LICENSE.md file.
"Unwish Twitter" is provided as is without any warranties and free of charge for all intents and purposes.